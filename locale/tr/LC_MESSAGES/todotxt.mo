��    0      �  C         (     )     @  I   X  4   �     �     �  #   �          1     :     ?     D     W     ^     o     �     �     �     �  ,   �     �               3     9  	   E     O     i     z     �     �     �     �     �     �     �  #   �          !     2  !   @  !   b     �     �  	   �  )   �     �  �  �     �	  +   �	  X   

  :   c
     �
     �
  8   �
  '   �
  
        '     .     7     F     M      a     �     �     �     �  =   �  
        %  %   >     d     s     �     �     �     �  	   �     �     �            $   ;     `  .   o     �     �     �  +   �  &   �          ,     H  1   ^     �         +                    #         )          .                   ,                        "          *      &   %            $   '                              !              
         -       	                          0      /   (    %(file) exists already Action on clicking task An error occured while trying to launch the default text editor: %(error) An error occurred while writing to %(file): %(error) Archive %(task) Are you sure? Auto-add creation date to new tasks Auto-archive done tasks Behavior Bold Both Bottom of the file Cancel Cannot open file Color done tasks Color for detected URLs Confirm task deletion Contexts Create new file Create todo.txt and done.txt file in %(path) Custom color Custom color for URLS Custom color for done tasks Debug Debug level Debugging Decrease %(task) priority Dedicated button Delete %(task) Detail Edit %(task) Error writing file Expand %(task) Increase %(task) priority Mark %(task) as done New task… No valid %(filename) file specified Ok Open preferences Open settings Open todo.txt file in text editor Please choose what you want to do Select location in settings Undo delete %(task) Ungrouped Unknown error during file write: %(error) Use existing file Project-Id-Version: Turkish (todo-txt-gnome-shell-extension)
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: Turkish <https://hosted.weblate.org/projects/todo-txt-gnome-shell-extension/extension-and-preferences/tr/>
Language: tr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 5.8-dev
 %(file) zaten mevcut Göreve tıklandığında yapılacak işlem Varsayılan metin düzenleyicisini başlatmaya çalışırken bir hata oluştu: %(error) %(file) dosyasına yazılırken bir hata oluştu: %(error) %(task) arşivle Emin misin? Yeni görevlere otomatik olarak oluşturulma tarihi ekle Tamamlanan görevleri otomatik arşivle Davranış Kalın İkiside Dosyanın sonu İptal Dosya açılamıyor Tamamlanan görevleri renklendir Algılanan URL'leri renklendir Görev silmeyi onayla Bağlam menüsü Yeni dosya oluştur %(path) dizininde todo.txt ve done.txt dosyalarını oluştur Özel renk URL'ler için özel renk Tamamlanan görevler için özel renk Hata ayıklama Hata ayıklama seviyesi Hata Ayıklama %(task) önceliğini düşür Özel buton %(task) sil Ayrıntı %(task) düzenle Dosya yazılırken hata oluştu %(task) genişlet %(task) önceliğini yükselt %(task) tamamlandı olarak işaretle Yeni görev… Geçerli bir %(filename) dosyası belirtilmedi Tamam Tercihleri aç Ayarları aç todo.txt dosyasını metin editöründe aç Lütfen ne yapmak istediğinizi seçin Ayarlarda konumu seçin %(task) silinmesini geri al Gruplandırılmamış Dosya yazma sırasında bilinmeyen hata: %(error) Mevcut dosyayı kullan 